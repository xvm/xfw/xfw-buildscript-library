# SPDX-License-Identifier: LGPL-3.0+
# Copyright (c) 2017-2022 XVM Team


function Create-Zip($Directory, $CompressionLevel=0)
{
    Push-Location $Directory

    Find-Zip -Required | Out-Null
    zip -$CompressionLevel -q -X -r ./output.zip ./*

    Pop-Location
}