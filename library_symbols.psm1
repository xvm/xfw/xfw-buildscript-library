# SPDX-License-Identifier: LGPL-3.0+
# Copyright (c) 2017-2022 XVM Team




function Upload-Symbols($Directory){
    Find-SentryCli -Required | Out-Null

    $token = ${env:XFW_SENTRY_AUTHTOKEN}
    $org = ${env:XFW_SENTRY_ORGANIZATION}
    $project = ${env:XFW_SENTRY_PROJECT}
    $url = ${env:XFW_SENTRY_URL}

    if(($null -eq $token) -or ($null -eq $org) -or ($null -eq $project)){
        return
    }

    sentry-cli --url $url --auth-token $token upload-dif --org $org --project $project "$Directory"
}
