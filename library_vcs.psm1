# SPDX-License-Identifier: LGPL-3.0+
# Copyright (c) 2017-2022 XVM Team


function Get-MercurialRepoStats($Path)
{
    Find-Mercurial -Required

    if($Path -eq $null)
    {
        $Path = $PWD.Path
    }

    #shitty hack: add leading zeros to rev to fix WoT version comparison algorithm
    $rev = $(hg parent --template "{rev}")
    $rev_zerocount = 5 - $rev.Length
    while ($rev_zerocount -gt 0) {
        $rev = "0" + $rev
        $rev_zerocount --
    }

    return  @{
        "Author"      = $(hg parent --template "{author}")
        "Branch"      = $(hg parent --template "{branch}")
        "Date"        = $(hg parent --template "{date|isodate}")
        "Description" = $(hg parent --template "{desc}")
        "Hash"        = $(hg parent --template "{node|short}")
        "Revision"    = $rev
        "Tags"        = $(hg parent --template "{tags}")
    }
}
