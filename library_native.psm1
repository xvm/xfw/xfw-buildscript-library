# SPDX-License-Identifier: LGPL-3.0+
# Copyright (c) 2017-2022 XVM Team

function Build-CmakeProject
{
    param (
        [string]$SourceDirectory,
        [string]$BuildDirectory,
        [string]$InstallDirectory,
        $PrefixDirectory = $null,
        [string]$ToolchainFile,
        [string]$Config = "RelWithDebInfo",
        [string]$Generator = "",
        [string]$Toolchain = "",
        [string[]]$Defines = $null
    )

    Write-Output "Building CMake Project: $SourceDirectory"

    #fill prefix directory
    if (!$PrefixDirectory){
        $PrefixDirectory = "$($InstallDirectory.Replace('\','/').TrimEnd('/'))/"
    }
    elseif($PrefixDirectory -is [array]){
        $PrefixDirectory = ($PrefixDirectory.Replace('\','/').TrimEnd('/') -join "/$Arch/;")
    }
    else{
        $PrefixDirectory = "$($PrefixDirectory.Replace('\','/').TrimEnd('/'))/"
    }

    #fix toolchain file
    if ($ToolchainFile){
        $ToolchainFile = $ToolchainFile.Replace('\','/')
    }

    #fix build directory
    $BuildDirectory = "$BuildDirectory/"
    $InstallDirectory = "$InstallDirectory/"

    #create directories
    Remove-Item -Recurse -Path $BuildDirectory -ErrorAction SilentlyContinue | Out-Null
    New-Item -ItemType Directory -Path $BuildDirectory -ErrorAction SilentlyContinue | Out-Null

    #1. generator
    if ($Generator){
        $Generator = "-G " + $Generator
    }
    else{
        $Generator = ""
    }

    #2. Toolchain
    if($Toolchain){
        $Toolchain = "-T " + $Toolchain
    }
    else {
        $Toolchain = ""
    }

    #generate msvs proj
    cmake $Generator $Toolchain -A x64 -S"$SourceDirectory" -B"$BuildDirectory" -DCMAKE_INSTALL_PREFIX="$InstallDirectory" -DCMAKE_PREFIX_PATH="$PrefixDirectory" -DCMAKE_TOOLCHAIN_FILE="$ToolchainFile" $Defines
    if ($LastExitCode -ne 0) {
        exit $LastExitCode
    }

    #build proj
    cmake --build "$BuildDirectory" --target INSTALL --config $Config
    if ($LastExitCode -ne 0) {
        exit $LastExitCode
    }
}
