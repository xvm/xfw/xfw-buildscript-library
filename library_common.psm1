# SPDX-License-Identifier: LGPL-3.0+
# Copyright (c) 2017-2022 XVM Team



function Get-Architecture()
{
    $os = Get-OS

    if($os -eq "windows")
    {
        switch(${env:PROCESSOR_ARCHITECTURE})
        {
            AMD64
            {
                return "amd64"
            }

            x86
            {
                return "i686"
            }
        }
    }
    elseif($os -eq "linux")
    {
        switch ($(uname -m))
        {
            x86_64
            {
                return "amd64"
            }
        }
    }

    return "Unknown"
}


function Get-OS()
{
    if($PSVersionTable.PSEdition -ne "Core")
    {
        return "windows"
    }

    if($PSVersionTable.OS.StartsWith("Microsoft"))
    {
        return "windows"
    }

    if($PSVersionTable.OS.StartsWith("Linux"))
    {
        return "linux"
    }

    return "unknown"
}


function Edit-Path ($Path, [switch] $Append, [switch] $Prepend)
{
    $os = Get-OS


    if($Append)
    {
        if($os -eq "windows")
        {
            ${env:PATH}="${env:PATH};${Path}"
        }
        else
        {
            ${env:PATH}="${env:PATH}:${Path}"
        }
    }

    if($Prepend)
    {
        if($os -eq "windows")
        {
            ${env:PATH}="${Path};${env:PATH}"
        }
        else
        {
            ${env:PATH}="${Path}:${env:PATH}"
        }
    }

}


function Write-ErrorLine($message) {
    [Console]::ForegroundColor = 'red'
    [Console]::Error.WriteLine($message)
    [Console]::ResetColor()
}


function Read-Config($Path = $null, $DefaultConfig = @{}){
    $result = $DefaultConfig.Clone()

    if($Path -and (Test-Path -Path $Path -PathType Leaf)){
        $ConfigFile = Get-Content -Path $Path | ConvertFrom-Json -AsHashtable
        foreach($kvp in $ConfigFile.GetEnumerator()){
            $result.($kvp.Key) = $kvp.Value
        }
    }

    return $result
}