# SPDX-License-Identifier: LGPL-3.0+
# Copyright (c) 2017-2022 XVM Team


function Find-Vcpkg([Switch] $Required){
    $appNames = @(
        "vcpkg",
        "C:\vcpkg\vcpkg.exe",
        "C:\tools\vcpkg\vcpkg.exe"
        "D:\vcpkg\vcpkg.exe"
    )

    $path = $null;
    foreach ($appname in $appNames) {
        $path = Find-Application $appname
        if(!$path){
            continue;
        }

        if($null -ne $path){
            break;
        }
    }

    if(!$path)
    {
        if($Required)
        {
            Write-Error -Message "vcpkg is required"
            exit 1
        }
        return $null
    }

    return $path
}


function Get-VcpkgToolchainFile(){
    $vcpkg = Find-Vcpkg
    if(!$vcpkg){
        return $null
    }

    $vcpkg_dir = Split-Path -Path $vcpkg -Parent
    return (Join-Path -Path $vcpkg_dir -ChildPath "scripts/buildsystems/vcpkg.cmake") -replace '\\','/'
}

function Get-VcpkgTriplet($TripletMask, $Architecture){
    if($Architecture -ieq "x86_32"){
        $Architecture = "x86-windows"
    }
    elseif($Architecture -ieq "x86_64"){
        $Architecture = "x64-windows"
    }

    return $TripletMask -replace "{arch}",$Architecture
}


function Install-VcpkgPackages($Packages, $Triplet){
    if(!$Packages){
        return
    }

    $vcpkg = Find-Vcpkg
    if(!$vcpkg){
        Write-ErrorLine -message "Vcpkg was not found"
        exit 1
    }

    $pkgs = @()
    foreach($package in $Packages){
        $pkgs += "$($package):$Triplet"
    }


    & $vcpkg install $pkgs
}