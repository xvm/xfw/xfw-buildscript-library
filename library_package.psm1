# SPDX-License-Identifier: LGPL-3.0+
# Copyright (c) 2017-2022 XVM Team

#
# Default Configuration
#

$default_config_package = @{
    id = ""
    name = ""
    description = ""
    version = "0.0.0"

    path_license =  $null

    build_components = @(".")
    build_devel = $true
    build_wotmod = $true
}

$default_config_assets = @{
    install_prefix = "res/"
}

$default_config_component = @{
    id = ""
    name = ""
    description = ""

    xfw_dependencies = @()
    xfw_prefix = $null
    xfw_features = @()
    xfw_features_provide = @()
    xfw_url = ""
    xfw_url_update = ""
    xfw_wot_versions = @("0.0.0.0")
    xfw_wot_versions_strategy = "allow_newer"
}

$default_config_native = @{
    build_defines = @()
    install_devel = $true
    install_xfw = $true
    vcpkg_packages = @()
    vcpkg_triplet = $null
    sdk_components = @()
}

$default_config_python = @{
    install_prefix = $null
    pypi_packages = $()
}



#
# File creation
#

function Create-WotMeta($PackageConfig, $FilePath){
    $content =
"<root>
    <id>$($PackageConfig.id)</id>
    <name>$($PackageConfig.name)</name>
    <description>$($PackageConfig.description)</description>
    <version>$($PackageConfig.version)</version>
</root>
"

    Out-File -FilePath $FilePath -Encoding utf8 -InputObject $content
}


function Create-XfwMeta($ComponentConfig, $FilePath){

    $result = @{
        id = $ComponentConfig.id
        name = $ComponentConfig.name
        description = $ComponentConfig.description
        version = $ComponentConfig.version
        dependencies = $ComponentConfig.xfw_dependencies
        features = $ComponentConfig.xfw_features
        features_provide = $ComponentConfig.xfw_features_provide
        url = $ComponentConfig.xfw_url
        url_update = $ComponentConfig.xfw_url_update
        wot_versions = $ComponentConfig.xfw_wot_versions
        wot_versions_strategy = $ComponentConfig.xfw_wot_versions_strategy
    }

    ConvertTo-Json $result | Set-Content -Path $FilePath
}



#
# Build
#

function Build-PackageComponent($PackageContext, $ComponentName) {
    Write-Output "  - component: $ComponentName"
    $pkg_context = $PackageContext
    $comp_path = $(Join-Path -Path $pkg_context.path_pkg -ChildPath $ComponentName -Resolve) -replace "\\","/"

    # context
    $comp_context = @{
        config = Read-Config -Path "$comp_path/_meta/component.json" -DefaultConfig $default_config_component
        path_comp = $comp_path
    }
    if(!$comp_context.config.id){
        $comp_context.config.id = $pkg_context.config.id
    }
    if(!$comp_context.config.name){
        $comp_context.config.name = $pkg_context.config.name
    }
    if(!$comp_context.config.description){
        $comp_context.config.description = $pkg_context.config.description
    }
    if(!$comp_context.config.version){
        $comp_context.config.version = $pkg_context.config.version
    }
    if($comp_context.config.xfw_prefix){
        $comp_context.path_out_xfw = Join-Path -Path $pkg_context.path_out_wotmod -ChildPath "res/mods/xfw_packages/$($comp_context.config.xfw_prefix)/"
    }

    # native
    foreach($native_path_src in $(Get-ChildItem -Path $comp_context.path_comp -Filter "native*" -Directory)){
        Write-Output "    - native: $native_path_src"

        # config
        $native_config = @{}
        if(Test-Path -Path "$native_path_src/_meta/native.json" -PathType Leaf){
            $native_config = Read-Config -Path "$native_path_src/_meta/native.json" -DefaultConfig $default_config_native
        }
        elseif (Test-Path -Path "$comp_path/_meta/native.json" -PathType Leaf){
            $native_config = Read-Config -Path "$comp_path/_meta/native.json" -DefaultConfig $default_config_native
        }
        else {
            $native_config = Read-Config -DefaultConfig $default_config_native
        }

        # Receive SDK
        Get-XfwSdk -Components $native_config.sdk_components -OutputDirectory $pkg_context.path_out_sdk

        # build
        $native_path_tgt     = Join-Path -Path $pkg_context.path_out_build -ChildPath "${ComponentName}_build/"
        $native_path_install = Join-Path -Path $pkg_context.path_out_build -ChildPath "${ComponentName}_install/"

        $native_build_defs = $native_config.build_defines.Clone()
        if($native_config.vcpkg_packages){
            $vcpkg_triplet = Get-VcpkgTriplet -TripletMask $native_config.vcpkg_triplet
            Install-VcpkgPackages -Packages $native_config.vcpkg_packages -Triplet $vcpkg_triplet
            $native_build_defs += "-DVCPKG_TARGET_TRIPLET=$vcpkg_triplet"
            $native_build_defs += "-DCMAKE_TOOLCHAIN_FILE=`"$(Get-VcpkgToolchainFile)`""
        }

        #build, perform build
        #TODO: add ability to configure prefix
        Build-CmakeProject `
            -SourceDirectory   "$native_path_src"`
            -BuildDirectory    "$native_path_tgt"`
            -InstallDirectory  "$native_path_install"`
            -PrefixDirectory   @($pkg_context.path_out_sdk, $pkg_context.path_out_devel)`
            -Defines            $native_build_defs

        #copy
        if($native_config.install_devel){
            Copy-Item -Path "$native_path_install/*" -Destination $pkg_context.path_out_devel -Recurse -Force
        }
        if($native_config.install_xfw){
            if(Test-Path -Path "$native_path_install/bin_lesta") {
                New-Item -Path "$($comp_context.path_out_xfw)/native_lesta/" -ItemType Directory -Force -ErrorAction SilentlyContinue | Out-Null
                Copy-Item -Path "$native_path_install/bin_lesta/*.dll" -Destination "$($comp_context.path_out_xfw)/native_lesta/" -Force
                Copy-Item -Path "$native_path_install/bin_lesta/*.exe" -Destination "$($comp_context.path_out_xfw)/native_lesta/" -Force
                Copy-Item -Path "$native_path_install/bin_lesta/*.pyd" -Destination "$($comp_context.path_out_xfw)/native_lesta/" -Force
            }
            if(Test-Path -Path "$native_path_install/bin_wg") {
                New-Item -Path "$($comp_context.path_out_xfw)/native_wg/" -ItemType Directory -Force -ErrorAction SilentlyContinue | Out-Null
                Copy-Item -Path "$native_path_install/bin_wg/*.dll" -Destination "$($comp_context.path_out_xfw)/native_wg/" -Force
                Copy-Item -Path "$native_path_install/bin_wg/*.exe" -Destination "$($comp_context.path_out_xfw)/native_wg/" -Force
                Copy-Item -Path "$native_path_install/bin_wg/*.pyd" -Destination "$($comp_context.path_out_xfw)/native_wg/" -Force
            }
            if(Test-Path -Path "$native_path_install/bin") {
                New-Item -Path "$($comp_context.path_out_xfw)/native_lesta/" -ItemType Directory -Force -ErrorAction SilentlyContinue | Out-Null
                New-Item -Path "$($comp_context.path_out_xfw)/native_wg/" -ItemType Directory -Force -ErrorAction SilentlyContinue | Out-Null

                Copy-Item -Path "$native_path_install/bin/*.dll" -Destination "$($comp_context.path_out_xfw)/native_lesta/" -Force
                Copy-Item -Path "$native_path_install/bin/*.exe" -Destination "$($comp_context.path_out_xfw)/native_lesta/" -Force
                Copy-Item -Path "$native_path_install/bin/*.pyd" -Destination "$($comp_context.path_out_xfw)/native_lesta/" -Force
                Copy-Item -Path "$native_path_install/bin/*.dll" -Destination "$($comp_context.path_out_xfw)/native_wg/" -Force
                Copy-Item -Path "$native_path_install/bin/*.exe" -Destination "$($comp_context.path_out_xfw)/native_wg/" -Force
                Copy-Item -Path "$native_path_install/bin/*.pyd" -Destination "$($comp_context.path_out_xfw)/native_wg/" -Force
            }
        }
    }

    # python
    foreach($comp_python_dirname in $(Get-ChildItem -Path $comp_context.path_comp -Filter "python*" -Directory)){
        Write-Output "    - python: $comp_python_dirname"
        
        # config
        $python_config = @{}
        if(Test-Path -Path "$comp_python_dirname/_meta/python.json" -PathType Leaf){
            $python_config = Read-Config -Path "$comp_python_dirname/_meta/python.json" -DefaultConfig $default_config_python
        }
        elseif (Test-Path -Path "$comp_path/_meta/python.json" -PathType Leaf){
            $python_config = Read-Config -Path "$comp_path/_meta/python.json" -DefaultConfig $default_config_python
        }
        else {
            $python_config = Read-Config -DefaultConfig $default_config_python
        }

        # output path
        if($python_config.install_prefix)
        {
            $python_config.install_prefix = $python_config.install_prefix -replace "{xfw_root}","res/mods/xfw_packages/$($comp_context.config.xfw_prefix)"
        }
        else{
            $python_config.install_prefix = "res/mods/xfw_packages/$($comp_context.config.xfw_prefix)/python/"
        }
        $python_path_out = Join-Path -Path $pkg_context.path_out_wotmod -ChildPath $python_config.install_prefix
        
        # pypi
        if($python_config.pypi_packages){
            foreach($package in $python_config.pypi_packages){
                Get-PypiPackage -Package $package -Path $python_path_out
            }
            Build-PythonDirectory -FileDirectory $python_path_out -OutputDirectory $python_path_out
            Remove-Item -Path $python_path_out -Filter "*.py" -Recurse -Force -ErrorAction SilentlyContinue
        }

        # build directory
        Build-PythonDirectory -FileDirectory $comp_python_dirname -OutputDirectory $python_path_out
    }

    # pathon empty __init__.pyc
    if($comp_context.path_out_xfw){
        if($(Test-Path $(Join-Path -Path $comp_context.path_out_xfw -ChildPath "python/") -PathType Container) -eq $true -and
            $(Test-Path $(Join-Path -Path $comp_context.path_out_xfw -ChildPath "__init__.pyc") -PathType Leaf) -eq $false){
            $temp_path_in = Join-Path $comp_context.path_out_xfw -ChildPath  "__init__.py"
            New-Item -Path $temp_path_in -ItemType File | Out-Null
            Build-PythonFile -FilePath $temp_path_in -OutputDirectory $comp_context.path_out_xfw
            Remove-Item -Path $temp_path_in | Out-Null
        }
    }

    # assets
    foreach($comp_assets_dirname in $(Get-ChildItem -Path $comp_context.path_comp -Filter "assets*" -Directory)){
        Write-Output "    - assets: $comp_assets_dirname"
        
        $assets_config = Read-Config -Path "$comp_assets_dirname/_meta/assets.json" -DefaultConfig $default_config_assets
        if($assets_config.install_prefix)
        {
            $assets_config.install_prefix = $assets_config.install_prefix -replace "{xfw_root}","res/mods/xfw_packages/$($comp_context.config.xfw_prefix)"
        }
        else{
            $assets_config.install_prefix = "res/mods/xfw_packages/$($comp_context.config.xfw_prefix)/assets/"
        }
        $assets_path_out = Join-Path -Path $pkg_context.path_out_wotmod -ChildPath $assets_config.install_prefix

        New-Item -Path $assets_path_out -ItemType Directory -Force -ErrorAction SilentlyContinue | Out-Null
        Copy-Item -Path (Get-Item -Path "$comp_assets_dirname/*" -Exclude ('_meta')).FullName -Destination $assets_path_out -Recurse -Force
    }

    # xfw meta
    if($comp_context.path_out_xfw){
        Create-XfwMeta -ComponentConfig $comp_context.config -FilePath "$($comp_context.path_out_xfw)/xfw_package.json"
    }
}

function Build-Package($PackageDirectory, $OutputDirectory) {
    #prepare
    New-Item -ItemType Directory -ErrorAction SilentlyContinue -Path "$OutputDirectory/" | Out-Null
    $PackageDirectory = $(Resolve-Path -Path $PackageDirectory) -replace "\\", "/"
    $OutputDirectory  = $(Resolve-Path -Path $OutputDirectory)  -replace "\\", "/"

    # config
    $pkg_context = @{
        config = Read-Config -Path (Join-Path -Path $PackageDirectory -ChildPath "_meta/package.json") -DefaultConfig $default_config_package

        path_pkg                 = $PackageDirectory

        path_out                  = $OutputDirectory
        path_out_build            = Join-Path -Path $OutputDirectory -ChildPath "build/"
        path_out_deploy           = Join-Path -Path $OutputDirectory -ChildPath "deploy/"
        path_out_wotmod           = Join-Path -Path $OutputDirectory -ChildPath "wotmod/"
        path_out_devel            = Join-Path -Path $OutputDirectory -ChildPath "devel/"
        path_out_sdk              = Join-Path -Path $OutputDirectory -ChildPath "sdk/"
    }

    Write-Output "- package: $($pkg_context.config.id)"

    # prepare, directories
    Remove-Item -Path "$($pkg_context.path_out_build)/"  -Force -Recurse -ErrorAction SilentlyContinue
    Remove-Item -Path "$($pkg_context.path_out_deploy)/" -Force -Recurse -ErrorAction SilentlyContinue
    Remove-Item -Path "$($pkg_context.path_out_wotmod)/" -Force -Recurse -ErrorAction SilentlyContinue
    Remove-Item -Path "$($pkg_context.path_out_devel)/"  -Force -Recurse -ErrorAction SilentlyContinue
    New-Item -ItemType Directory -ErrorAction SilentlyContinue -Path "$($pkg_context.path_out)/"        | Out-Null
    New-Item -ItemType Directory -ErrorAction SilentlyContinue -Path "$($pkg_context.path_out_build)/"  | Out-Null
    New-Item -ItemType Directory -ErrorAction SilentlyContinue -Path "$($pkg_context.path_out_deploy)/" | Out-Null
    New-Item -ItemType Directory -ErrorAction SilentlyContinue -Path "$($pkg_context.path_out_wotmod)/" | Out-Null
    New-Item -ItemType Directory -ErrorAction SilentlyContinue -Path "$($pkg_context.path_out_devel)/"  | Out-Null
    New-Item -ItemType Directory -ErrorAction SilentlyContinue -Path "$($pkg_context.path_out_sdk)/"    | Out-Null

    # process, components
    foreach ($comp_name in $pkg_context.config.build_components) {
        Build-PackageComponent -PackageContext $pkg_context -ComponentName $comp_name
    }

    # wotmod
    if($pkg_context.config.build_wotmod){
        # license
        if($pkg_context.config.path_license){
            Copy-Item -Path (Join-Path -Path $pkg_context.path_pkg -ChildPath $pkg_context.config.path_license) -Destination "$($pkg_context.path_out_wotmod)/LICENSE.md"
        }

        # meta
        Create-WotMeta -PackageConfig $pkg_context.config -FilePath "$($pkg_context.path_out_wotmod)/meta.xml"

        # other content
        Create-Zip -Directory $pkg_context.path_out_wotmod
        Move-Item "$($pkg_context.path_out_wotmod)/output.zip" "$($pkg_context.path_out_deploy)/$($pkg_context.config.id)_$($pkg_context.config.version).wotmod" -Force -ErrorAction SilentlyContinue
    }

    # devel
    if($pkg_context.config.build_devel) {
        Create-Zip -Directory $pkg_context.path_out_devel -CompressionLevel 9
        Move-Item "$($pkg_context.path_out_devel)/output.zip" "$($pkg_context.path_out_deploy)/$($pkg_context.config.id)_$($pkg_context.config.version)-devel.zip" -Force -ErrorAction SilentlyContinue
    }
}
