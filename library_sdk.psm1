# SPDX-License-Identifier: LGPL-3.0+
# Copyright (c) 2017-2022 XVM Team

function Download-NativeDevelPackage($OutputPath)
{
    $link = (Invoke-WebRequest "https://gitlab.com/xvm/xfw/xfw.native/-/raw/master/_latest_devel.txt" -UseBasicParsing).Content
    Invoke-WebRequest "$link" -OutFile devel.zip
    Expand-Archive -Path ./devel.zip -DestinationPath "$OutputPath"
    Remove-Item -Path "./devel.zip"
}

function Get-XfwSdk($Components, $OutputDirectory, $CacheDirectory = $null){
    $repo_root = "https://dev.modxvm.com/sdk/"

    # parse arguments
    $components_target = $null
    if(!$Components){
        return
    }
    elseif($Components -is [string]){
        $components_target = @{$Components = "latest"}
    }
    elseif($Components -is [array]){
        $components_target = @{}
        foreach($component in $Components){
            $components_target.$component = "latest"
        }
    }
    if(!$CacheDirectory){
        $CacheDirectory = Join-Path -Path $OutputDirectory -ChildPath "_cache/"
    }

    # create output directory
    New-Item -Path $OutputDirectory -ItemType Directory -Force -ErrorAction SilentlyContinue  | Out-Null
    New-Item -Path $CacheDirectory  -ItemType Directory -Force -ErrorAction SilentlyContinue  | Out-Null

    # download meta with avaialable components
    $components_available = ((Invoke-WebRequest "$repo_root/meta.json").Content | ConvertFrom-Json).components

    # process required components
    foreach($component in $components_target.GetEnumerator()){

        # find it on server
        $component_found = $false
        foreach($iter in $components_available){
            if($iter.id -ieq $component.Key){
                $component_found = $true
                break
            }
        }
        if(!$component_found){
            Write-ErrorLine "Component $($component.Key) is not available"
            return $false
        }

        # download meta
        $component_meta = ((Invoke-WebRequest "$repo_root/$($component.Key)/meta.json").Content | ConvertFrom-Json)

        # fixup verison

        if($component.Value -ieq "latest"){
            $component.Value = $component_meta.latest
        }
        $component_hash = $component_meta.devel.($component.Value)

        # check cache
        $component_filename = "$($component.Key)_$($component.Value)-devel.zip"
        $component_filepath = $(Join-Path -Path $CacheDirectory -ChildPath $component_filename)
        $component_valid = $false
        if(Test-Path -Path $component_filepath -PathType Leaf){
            $hash_calculated = (Get-FileHash -Path $component_filepath -Algorithm SHA256).Hash
            if($hash_calculated -ieq $component_hash){
                $component_valid = $true
            }
        }

        # download file
        if(!$component_valid){
            $component_url = "$repo_root/$($component.Key)/$component_filename"
            Invoke-WebRequest -Uri $component_url -OutFile $component_filepath
        }

        # expand
        Expand-Archive -Path $component_filepath -DestinationPath $OutputDirectory -Force
    }
}