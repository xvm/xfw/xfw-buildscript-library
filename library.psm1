# SPDX-License-Identifier: LGPL-3.0+
# Copyright (c) 2017-2022 XVM Team

#
# Check PowerShell version
#

if($PSVersionTable.PSVersion -lt "7.2"){
    [Console]::ForegroundColor = 'red'
    [Console]::Error.WriteLine("This script requires Powershell 7.2")
    [Console]::ResetColor()
    exit 1 
}



#
# Load modules
#

foreach($module in Get-ChildItem -Path $PSScriptRoot -Filter "library_*.psm1"){
    Import-Module $module -Force -DisableNameChecking
}
