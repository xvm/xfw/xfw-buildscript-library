# SPDX-License-Identifier: LGPL-3.0+
# Copyright (c) 2017-2023 XVM Team

function Get-XvmbuildVersion() {
    return $(Get-Content -Path $(Join-Path -Path $PSScriptRoot -ChildPath "version.txt"))
}
